package hexa.initiative.dagger2mitchcoding;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import hexa.initiative.dagger2mitchcoding.di.components.DaggerAppComponent;

public class BaseApplication extends DaggerApplication {
    BaseApplication baseApplication = this;
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(baseApplication).build();
    }
}

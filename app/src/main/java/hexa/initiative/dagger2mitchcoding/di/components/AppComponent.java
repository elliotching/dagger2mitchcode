package hexa.initiative.dagger2mitchcoding.di.components;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import hexa.initiative.dagger2mitchcoding.BaseApplication;
import hexa.initiative.dagger2mitchcoding.di.modules.ActivityBuildersModule;
import hexa.initiative.dagger2mitchcoding.di.modules.AppModule;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuildersModule.class,
                AppModule.class,
        }
)
public interface AppComponent extends AndroidInjector<BaseApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}

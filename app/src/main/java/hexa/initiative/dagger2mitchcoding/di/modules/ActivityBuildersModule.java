package hexa.initiative.dagger2mitchcoding.di.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import hexa.initiative.dagger2mitchcoding.activities.AuthActivity;

@Module
public abstract class ActivityBuildersModule {
    // somehow called [ActivityInjectorModule]

    @ContributesAndroidInjector
    abstract AuthActivity contributeAuthActivity();
}
